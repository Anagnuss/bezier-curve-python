import tkinter
from typing import List, Tuple


class Vector2:
    def __init__(self, x: int, y: int):
        self.x, self.y = x, y

    def __add__(self, other):
        if isinstance(other, Vector2):
            return Vector2(self.x + other.x, self.y + other.y)
        elif isinstance(other, float) or isinstance(other, int):
            return Vector2(self.x + other, self.y + other)
        return None

    def __sub__(self, other):
        if isinstance(other, Vector2):
            return Vector2(self.x - other.x, self.y - other.y)
        elif isinstance(other, float) or isinstance(other, int):
            return Vector2(self.x - other, self.y - other)
        return None

    def __mul__(self, other):
        if isinstance(other, Vector2):
            return Vector2(self.x * other.x, self.y * other.y)
        elif isinstance(other, float) or isinstance(other, int):
            return Vector2(self.x * other, self.y * other)
        return None

    def __str__(self):
        return "x:" + str(self.x) + " y:" + str(self.y)

    def __round__(self, n=None):
        return Vector2(round(self.x, n), round(self.y, n))

    def get_pos(self) -> (float, float):
        return self.x, self.y


def Vlerp(v1: Vector2, v2: Vector2, t: float) -> Vector2:
    return v1 * (1 - t) + v2 * t


def optimize_int_list(ls: List[float]) -> List[int]:
    ls2 = [round(num) for num in ls]
    res = []
    for i in ls2:
        if i not in res:
            res.append(i)
    return res


def optimize_int_vector_list(ls: List[Vector2]) -> List[Vector2]:
    ls2 = [num.__round__() for num in ls]
    res = []
    for i in ls2:
        if i not in res:
            res.append(i)
    return res


def draw_lines_complex(canvas: tkinter.Canvas, ls: List[Vector2]) -> List[int]:
    ld = []
    for i in range(1, len(ls)):
        ld.append(canvas.create_line(ls[i - 1].get_pos(), ls[i].get_pos(), width=5))
    return ld


def calc_bezeval(ls: List[Vector2], precision: int, canvas: tkinter.Canvas) -> Tuple[List[int], List[Vector2]]:
    if len(ls) == 1:
        return None
    elif len(ls) == 2:
        return draw_lines_complex(canvas, ls), ls
    elif len(ls) > 2:
        p = []
        for i in range(0, precision + 1):
            t = i / precision
            p.append(Blevel(ls, t))
        ld = p.copy()
        lf = optimize_int_vector_list(ld)
        return draw_lines_complex(canvas, lf), lf  # replace lf -> p


def Blevel(ls: List[Vector2], t: int) -> Vector2:
    if len(ls) == 1:
        return ls[0]
    o = []
    for p in range(1, len(ls)):
        o.append(Vlerp(ls[p - 1], ls[p], t))
    return Blevel(o, t)


