import tkinter
from typing import List

import flib
from flib import Vector2

screen_width = 800
screen_heigth = 800

precision = 200

canvas = tkinter.Canvas(width=screen_width, height=screen_heigth)
canvas.pack()

c_collector = []
c_state = 0

line_ids = [int]
points = [Vector2]
adjust_points = [Vector2]

ln = [Vector2]

holding = False
holding_adj_point = 0
holding_point_ids = []

c_x, c_y = 0, 0
c_dx, c_dy = 0, 0

start_point = Vector2(100, 300)
end_point = Vector2(500, 300)

canvas.create_oval((start_point + 3).get_pos(), (start_point - 3).get_pos(), fill="red",
                   outline="")  # create start point circle
canvas.create_oval((end_point + 3).get_pos(), (end_point - 3).get_pos(), fill="red",
                   outline="")  # create end point circle

line_ids.clear()
points.clear()
adjust_points.clear()
line_ids, points = flib.calc_bezeval([start_point, end_point, end_point], precision, canvas)


# print(len(points))


# flib.calc_bezeval(ln, 1000, canvas)

def arround(a: int, val: int, range: int):
    return a + range > val > a - range


def on_click(cords) -> None:
    global points, adjust_points, holding, holding_adj_point, holding_point_ids
    x, y = cords.x, cords.y
    vd = Vector2(x, y)

    if holding:
        holding = False
        return

    rond = 0
    rang = 10  # search len # 10
    oval_draw_len = 5

    i = 0
    for a in adjust_points:
        i += 1
        if arround(a.get_pos()[0], x, oval_draw_len) and arround(a.get_pos()[1], y, oval_draw_len):
            holding = True
            holding_adj_point = i - 1
            return

    found = False
    fld = 0
    for vc in points:
        if arround(round(vc.get_pos()[0], rond), round(x, rond), rang) and arround(round(vc.get_pos()[1], rond),
                                                                                   round(y, rond), rang):
            found = True
            vd = Vector2(round(vc.get_pos()[0], rond), round((vc.get_pos())[1], rond))
            break
        fld += 1
    if found:
        if len(adjust_points) != 0:
            f = len(points) / len(adjust_points)
        else:
            f = len(points)
        d = 0
        n = 0
        while d < fld:
            d += f
            n += 1
        # d -= f
        n -= 1

        # print(len(adjust_points), len(points), n)

        # adjust_points.append(vd)
        # holding_point_ids.append(
        #    canvas.create_oval((vd + oval_draw_len).get_pos(), (vd - oval_draw_len).get_pos(), fill="red", outline="")
        # )
        # FIXME: don´t append at end but in correct place

        adjust_points.insert(n, vd)
        holding_point_ids.insert(n,
                canvas.create_oval((vd + oval_draw_len).get_pos(), (vd - oval_draw_len).get_pos(),
                fill="red", outline="")
        )




def fprint(o: List[Vector2]):
    print("[\"" + str(o[0]) + "\"", end="")
    for s in o[1:]:
        print(",\"" + str(s) + "\"", end="")
    print("]")


def mouse_move(cords: tkinter.Event):
    global adjust_points, holding, holding_adj_point, holding_point_ids, canvas
    if holding:
        canvas.move(holding_point_ids[holding_adj_point], cords.x - adjust_points[holding_adj_point].get_pos()[0],
                    cords.y - adjust_points[holding_adj_point].get_pos()[1])
        adjust_points[holding_adj_point] = Vector2(cords.x, cords.y)
        recalc()


def recalc():
    global canvas, line_ids, points, adjust_points
    dell_all(canvas, line_ids)
    line_ids, points = flib.calc_bezeval(([start_point, ] + adjust_points + [end_point, ]), precision, canvas)


def dell_all(canv: tkinter.Canvas, dl: List[int]):
    for x in dl:
        canv.delete(x)


canvas.bind_all("<1>", on_click)
canvas.bind_all("<Motion>", mouse_move)
tkinter.mainloop()
